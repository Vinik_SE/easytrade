﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class Ressource
        {
            public List<RessourceToken> ressourceList;
            //TODO : Integrate
            struct TradeResource
            {
                //Количество, при котором стоимость ресурса равна 1
                //the amount of the resource for it to cost 1 price unit
                public double C;
                //текущее количество ресурса, значение надо обновлять перед каждой трансакцией
                //the current amount of resource, bust be renewed before each transaction
                public double x0;
                //выгодность курса на покупку станцией в глазах игрока
                //больше значение - выгодней сдавать ресурс в обмен на другой ресурс
                //the profitability rate of the resource
                //the bigger value - the more profitable is to sell this resource to the station
                public double buyRate()
                {
                    return C / x0;
                }
                //станция продает ресурс за dx другого ресурса
                //station sells the resource for dx of other resource r
                public double sell(ref TradeResource r, double dx)
                {
                    return sell(r.buy(dx));
                }
                //станция продает ресурс на стоимость p - возвращает количество ресурса
                //station sells the resource for price p - method returns the amount of resouce
                public double sell(double p)
                {
                    double x = Math.Exp((C * Math.Log(x0) - p) / C);
                    double dx = x0 - x;
                    //x0 = x;
                    return dx;
                }
                //станция покупает dx ресурса - возвращает стоимость, за которую покупает
                //station buys dx of resource - method returns the price for which the station buys
                public double buy(double dx)
                {
                    double x = x0 + dx;
                    double p = (-C * Math.Log(x0) + C * Math.Log(x));
                    //x0 = x;
                    return p;
                }
            }

            public Ressource()
            {
                ressourceList = new List<RessourceToken>();
            }

            

            public double GetRessourcePrice( string aname, string atype )
            {
                foreach ( RessourceToken res in ressourceList )
                {
                    if ( res.Name == aname && res.Type == atype )
                    {
                        return res.Price;
                    }
                }

                //ELSE RETURN 0 if no ressource
                return 0;
            }

            public void createRessource(string name, string type, double price)
            {
                ressourceList.Add(new RessourceToken(name, type, price));
            }
            
        }            
    }
}
