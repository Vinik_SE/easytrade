﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // This file contains your actual script.
        //
        // You can either keep all your code here, or you can create separate
        // code files to make your program easier to navigate while coding.
        //
        // In order to add a new utility class, right-click on your project, 
        // select 'New' then 'Add Item...'. Now find the 'Space Engineers'
        // category under 'Visual C# Items' on the left hand side, and select
        // 'Utility Class' in the main area. Name it in the box below, and
        // press OK. This utility class will be merged in with your code when
        // deploying your final script.
        //
        // You can also simply create a new utility class manually, you don't
        // have to use the template if you don't want to. Just do so the first
        // time to see what a utility class looks like.
        // 
        // Go to:
        // https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
        //
        // to learn more about ingame scripts.

        // UNMINIFIED STUFF
        //GLOBAL VARIABLES
        //double priceIndexer = 1;        
        double cobaltRatio = 26.51, cobaltIngotRatio = 88.38;
        double goldRatio = 87.85, goldIngotRatio = 8785.14; 
        double ironRatio = 0.99, ironIngotRatio = 1.0;
        double nickelRatio = 29.16, nickelIngotRatio = 72.91;
        double silverRatio = 72.91, silverIngotRatio = 729.16;
        double siliconRatio = 17.5, siliconIngotRatio = 25.0;
        double platinumRatio = 79.54, platinumIngotRatio = 15909.09;
        double uraniumRatio = 291.66, uraniumIngotRatio = 29166.66;
        double iceRatio = 0.83; 
        double magnesiumRatio = 36.45, magnesiumIngotRatio = 3472.2;
        double stoneRatio = 0.56, stoneIngotRatio = 0.62;
        double thoriumIngotRatio = 58415.24, thoriumRatio = 577.45;


        int cycleCount;
        bool initializer = false;

        List<IMyBlockGroup> TS_BlockGroups = new List<IMyBlockGroup>();

        Dictionary<string, Action> _commands = new Dictionary<string, Action>(StringComparer.OrdinalIgnoreCase);
        MyCommandLine _commandLine = new MyCommandLine();

        List<IMyCargoContainer> TradeStation_CoreCargo;
        List<IMyTextPanel> TradeStation_Core_LCD;
        List<IMyConveyorSorter> TradeStation_Core_Sorter;
        List<IMyTextPanel> TradeConnector_LCD;
        List<IMyTextPanel> UserList_LCD;
        List<IMyCargoContainer> TradeConnector_Cargo;
        List<IMyConveyorSorter> TradeConnector_Sorter;        
        List<IMySensorBlock> TradeConnector_Sensor;
        List<MyDetectedEntityInfo> entity_list;

        Ressource ressourceList;

        List<User> userList = new List<User>();        
        string[] userListToSave = new string[250];

        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script.
            //
            // The constructor is optional and can be removed if not
            // needed.
            //
            // It's recommended to set RuntimeInfo.UpdateFrequency
            // here, which will allow your script to run itself without a
            // timer block.

            //Set frequency to 100
            //Runtime.UpdateFrequency = UpdateFrequency.Update100;
            //Setup commands
            _commands["trade"] = Trade;
            _commands["refresh"] = Refresh;

            if (Storage != null && Storage != "")
            {
                string[] storedData = Storage.Split(';');
                // If there's at least one item in the storage data...
                if (storedData.Count() >= 1)
                {
                    // Retrieve first item. C# arrays are 0-indexed, meaning the first item in the list is item 0.
                    //_userListName = storedData[0];
                    for (int i = 0; i < storedData.Count(); i++)
                    {
                        if (storedData[i] != null && storedData[i] != "")
                        {
                            Echo("Stored Data" + storedData[i]);                           
                            userList.Add(new User(Convert.ToInt64(storedData[i].Split('_')[0]), storedData[i].Split('_')[1], Convert.ToDouble(storedData[i].Split('_')[2]), i));
                        }
                    }
                }
            }
            else
            {
                //Setup 1st Storage usage
                Storage = "";
            }
        }

        public void Save()
        {
            int i = 0;
            // Combine the state variables into a string separated by the ';' character
            foreach (User cUser in userList)
            {
                userListToSave[i++] = string.Join("_", cUser.Id, cUser.Name, cUser.Money);
            }

            //Set into STORAGE string
            Storage = string.Join(";", userListToSave, 0, i);
        }

        public void Main(string argument, UpdateType updateSource)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked,
            // or the script updates itself. The updateSource argument
            // describes where the update came from. Be aware that the
            // updateSource is a  bitfield  and might contain more than
            // one update type.
            //
            // The method itself is required, but the arguments above
            // can be removed if not needed.
            // block declarations

            if (!initializer)
            {
                //SETUP CUSTOM DATAS
                if (Me.CustomData == "")
                {
                    Me.CustomData = "[SETUP BLOCK NAMES FOR SPECIAL ACTIONS]\n";
                    Me.CustomData += "[LCD group name for Debug screens]\nLCDDebug\n";
                    Me.CustomData += "[LCD block name for main User List screen]\nLCDMainUser\n";
                    Me.CustomData += "[LCD group name for User List screens]\nLCDUserList\n";
                    Me.CustomData += "[Config: Exchange Rate, Add following this format]\n";
                    Me.CustomData += "Iron ingot:" + ironIngotRatio + "\nIron ore:" + ironRatio + "\n";
                    Me.CustomData += "Nickel ingot:" + nickelIngotRatio + "\nNickel ore:" + nickelRatio + "\n";
                    Me.CustomData += "Silicon ingot:" + siliconIngotRatio + "\nSilicon ore:" + siliconRatio + "\n";
                    Me.CustomData += "Cobalt ingot:" + cobaltIngotRatio + "\nCobalt ore:" + cobaltRatio + "\n";
                    Me.CustomData += "Magnesium powder:" + magnesiumIngotRatio + "\nMagnesium ore:" + magnesiumRatio + "\n";
                    Me.CustomData += "Silver ingot:" + silverIngotRatio + "\nSilver ore:" + silverRatio + "\n";
                    Me.CustomData += "Gold ingot:" + goldIngotRatio + "\nGold ore:" + goldRatio + "\n";
                    Me.CustomData += "Platinum ingot:" + platinumIngotRatio + "\nPlatinum ore:" + platinumRatio + "\n";
                    Me.CustomData += "Uranium ingot:" + uraniumIngotRatio + "\nUranium ore:" + uraniumRatio + "\n";
                    Me.CustomData += "Gravel ingot:" + stoneIngotRatio + "\nStone ore:" + stoneRatio + "\n";
                    Me.CustomData += "Ice ore:" + iceRatio + "\n";
                    Me.CustomData += "Thorium ingot:" + thoriumIngotRatio + "\nThorium ore:" + thoriumRatio;
                    Echo("PB Custom Data Setup Done!");
                }

                //ANALYSE BLOCK GROUPS TO CREATE SYSTEM
                GridTerminalSystem.GetBlockGroups(TS_BlockGroups);

                //CYCLE CORE GROUPS
                foreach (var blockGroup in TS_BlockGroups)
                {
                    string[] GroupNameString = blockGroup.Name.Split('_');

                    //IF WE GET Trade Station GROUPS
                    if (GroupNameString[0] == "EasyTrade")
                    {
                        //INSERT CUSTOM DATA ON 1ST LAUNCH
                        if (GroupNameString[1] == "TradeCargo")
                        {
                            List<IMyTextPanel> lcdList = new List<IMyTextPanel>();
                            blockGroup.GetBlocksOfType<IMyTextPanel>(lcdList);

                            foreach (IMyTextPanel lcd in lcdList)
                            {
                                if (lcd.CustomData == "PlayerChoice")
                                {
                                    //lcd.WriteText("Put your ressources in the collector/cargo");
                                    refreshPlayerChoice(lcd);
                                    Echo("Cargo found with LCD, Setup Done !");
                                }
                            }
                        }
                    }
                }

                Echo("Initializer Done !");
                Echo("Look my Custom Data, Make your Group.");
                Echo("Need Group: EasyTrade_CoreCargo");
                Echo("Need Group: EasyTrade_TradeCargo_XX (choose your number)");
                initializer = true;
            }
            else
            {


                Echo("Running cycle " + cycleCount++);

                ressourceList = new Ressource();

                //Cycle parameters in PB Custom Data
                string[] scriptParameters = Me.CustomData.Split(new string[] { "\n" }, StringSplitOptions.None);
                for (int i = 0; i < scriptParameters.Count(); i++)
                {
                    switch (scriptParameters[i])
                    {
                        case "[LCD group name for Debug screens]":
                            bool groupFind = false;
                            foreach (var blockGroup in TS_BlockGroups)
                            {
                                string[] GroupNameString = blockGroup.Name.Split('_');

                                //IF WE GET Trade Station GROUPS
                                if (GroupNameString[0] == "EasyTrade")
                                {
                                    //INSERT CUSTOM DATA ON 1ST LAUNCH
                                    if (GroupNameString[1] == scriptParameters[i + 1])
                                    {

                                        List<IMyTextPanel> lcdList = new List<IMyTextPanel>();
                                        blockGroup.GetBlocksOfType<IMyTextPanel>(lcdList);

                                        foreach (IMyTextPanel LCD_debug in lcdList)
                                        {
                                            try
                                            {
                                                writeRuntime(LCD_debug);
                                                groupFind = true;
                                            }
                                            catch (Exception ex)
                                            {
                                                Echo("Debug LCD Error, name :" + scriptParameters[i + 1]);
                                                printErrorDebug(ex);
                                            }
                                        }
                                    }
                                }
                            }
                            if ( !groupFind )
                            {
                                Echo("NO LCD DEBUG GROUP FOUND. (optional)");
                            }
                            break;
                        case "[LCD block name for main User List screen]":
                            try
                            {
                                IMyTextPanel LCD_user = GridTerminalSystem.GetBlockWithName(scriptParameters[i + 1]) as IMyTextPanel;

                                UserList_LCD = new List<IMyTextPanel>();
                                foreach (var blockGroup in TS_BlockGroups)
                                {
                                    string[] GroupNameString = blockGroup.Name.Split('_');

                                    //IF WE GET TRADE CONNECTORS GROUPS
                                    if (GroupNameString[0] == "EasyTrade")
                                    {
                                        if (GroupNameString[1] == scriptParameters[i + 3])
                                        {
                                            List<IMyTextPanel> currentBlockLCDGroup = new List<IMyTextPanel>();
                                            blockGroup.GetBlocksOfType<IMyTextPanel>(currentBlockLCDGroup);

                                            foreach (IMyTextPanel txt in currentBlockLCDGroup)
                                            {
                                                UserList_LCD.Add(txt);
                                            }
                                        }
                                    }
                                }
                                string command = "";
                                if (LCD_user != null)
                                {
                                    if (LCD_user.GetText() != "")
                                    {
                                        string[] detNew = LCD_user.GetText().Split(new string[] { "\n" }, StringSplitOptions.None);
                                        command = detNew[0].Split(':')[1];
                                    }

                                    if (command == "reset")
                                    {
                                        string[] text = LCD_user.GetText().Split(new string[] { "\n" }, StringSplitOptions.None);

                                        if (text.Count() >= 2)
                                        {
                                            userList = new List<User>();
                                            int counter = 0;
                                            for (int j = 1; j < text.Count(); j++)
                                            {

                                                string[] currentUser = text[j].Split(':');
                                                userList.Add(new User(Convert.ToInt64(currentUser[3]), currentUser[0], Convert.ToDouble(currentUser[1]), j - 1));
                                                userListToSave[j - 1] = string.Join("_", Convert.ToInt64(currentUser[3]), currentUser[0], Convert.ToDouble(currentUser[1]));
                                                counter = j;
                                            }

                                            //Set into STORAGE string
                                            Storage = string.Join(";", userListToSave, 0, counter);
                                        }
                                        else
                                        {
                                            Storage = "";
                                        }
                                    }
                                    else if (userList.Count() > 0)
                                    {
                                        if (UserList_LCD.Count() >= 1)
                                            DrawOnLCDList(UserList_LCD, "USER LIST:\n");
                                        else
                                            LCD_user.WriteText("USER LIST:\n");

                                        for (int j = 0; j < userList.Count(); j++)
                                        {
                                            //Cycle screens to write
                                            if (UserList_LCD.Count() >= 1)
                                                DrawOnLCDList(UserList_LCD, userList.ElementAt(j).Name + ":" + userList.ElementAt(j).Money + ":$TS ID:" + userList.ElementAt(j).Id + "\n", true);
                                            else
                                                LCD_user.WriteText(userList.ElementAt(j).Name + ":" + userList.ElementAt(j).Money + ":$TS ID:" + userList.ElementAt(j).Id + "\n", true);
                                        }
                                    }
                                    else
                                    {
                                        if (UserList_LCD.Count() >= 1)
                                        {
                                            DrawOnLCDList(UserList_LCD, "USER LIST:\n");
                                        }
                                        else
                                        {
                                            LCD_user.WriteText("USER LIST:\n");
                                        }
                                    }
                                }
                                else
                                {
                                    Echo("NO LCD FOR USER DEFINED!");
                                }
                            }
                            catch (Exception ex)
                            {
                                Echo("USER LIST ERROR, name : " + scriptParameters[i]);
                                printErrorDebug(ex);
                            }
                            break;
                        case "[Config: Exchange Rate, Add following this format]":
                            //string[] cData = Me.CustomData.Split(new string[] { "\n" }, StringSplitOptions.None);
                            for (int j = i + 1; j < scriptParameters.Count(); j++)
                            {
                                string[] ressource = scriptParameters[j].Split(':');
                                double resPrice = Convert.ToDouble(ressource[1]);
                                string resName = CapitalizeFirstLetter(ressource[0].Split(' ')[0]);
                                string resType = CapitalizeFirstLetter(ressource[0].Split(' ')[1]);
                                ressourceList.createRessource(resName, resType, resPrice);
                            }
                            break;
                        default:
                            //Debug -- Echo("DEFAULT CASE "+scriptParameters[i]);
                            break;
                    }
                }

                TradeStation_CoreCargo = new List<IMyCargoContainer>();
                TradeStation_Core_LCD = new List<IMyTextPanel>();
                TradeStation_Core_Sorter = new List<IMyConveyorSorter>();
                TradeConnector_LCD = new List<IMyTextPanel>();
                TradeConnector_Sorter = new List<IMyConveyorSorter>();
                TradeConnector_Sensor = new List<IMySensorBlock>();
                TradeConnector_Cargo = new List<IMyCargoContainer>();
                entity_list = new List<MyDetectedEntityInfo>();

                //ANALYSE BLOCK GROUPS TO CREATE SYSTEM
                GridTerminalSystem.GetBlockGroups(TS_BlockGroups);

                //CYCLE GROUPS
                foreach (var blockGroup in TS_BlockGroups)
                {
                    string[] GroupNameString = blockGroup.Name.Split('_');

                    //IF WE GET GROUPS
                    if (GroupNameString[0] == "EasyTrade")
                    {
                        //Close all Sorter
                        List<IMyConveyorSorter> currentBlockSorterGroup = new List<IMyConveyorSorter>();
                        blockGroup.GetBlocksOfType<IMyConveyorSorter>(currentBlockSorterGroup);

                        foreach (IMyConveyorSorter sorter in currentBlockSorterGroup)
                        {
                            try
                            {
                                sorter.ApplyAction("OnOff_Off");
                            }
                            catch (Exception ex)
                            {
                                Echo("ERROR APPLY ACTION ON SORTER");
                                printErrorDebug(ex);
                            }
                        }

                        //IF CORE CARGO GROUP, DO
                        if (GroupNameString[1] == "CoreCargo")
                        {
                            //LOOP ALL BLOCKS OF CORE CARGO GROUPS

                            //Create list of blocks for IMyCargoContainer group
                            List<IMyCargoContainer> currentBlockCargoGroup = new List<IMyCargoContainer>();
                            blockGroup.GetBlocksOfType<IMyCargoContainer>(currentBlockCargoGroup);

                            foreach (IMyCargoContainer cargo in currentBlockCargoGroup)
                            {
                                TradeStation_CoreCargo.Add(cargo);
                            }

                            //Create list of blocks for LCD group
                            List<IMyTextPanel> currentBlockLCDGroup = new List<IMyTextPanel>();
                            blockGroup.GetBlocksOfType<IMyTextPanel>(currentBlockLCDGroup);

                            foreach (IMyTextPanel lcd in currentBlockLCDGroup)
                            {
                                TradeStation_Core_LCD.Add(lcd);
                            }

                            List<IMyConveyorSorter> currentBlockSorterGroup1 = new List<IMyConveyorSorter>();
                            blockGroup.GetBlocksOfType<IMyConveyorSorter>(currentBlockSorterGroup1);
                            foreach (IMyConveyorSorter sorter in currentBlockSorterGroup1)
                            {
                                TradeStation_Core_Sorter.Add(sorter);
                            }
                        }
                    }
                }


                //DRAW CORE CARGO INVENTORIES
                DrawOnLCDList(TradeStation_Core_LCD, "");
                foreach (IMyCargoContainer myCargo in TradeStation_CoreCargo)
                {
                    //Draw Core Inventory
                    drawInventoryOnLCD(myCargo, TradeStation_Core_LCD.ElementAt(0));
                }

                //CYCLE AND EXECUTE COMMANDS
                if (_commandLine.TryParse(argument))
                {
                    Action commandAction;
                    var command = _commandLine.Argument(0);

                    if (command == null)
                    {
                        Echo("NO COMMAND");
                    } // TRY TO LAUNCH COMMAND
                    else if (_commands.TryGetValue(command, out commandAction))
                    {
                        try
                        {
                            commandAction();
                        }
                        catch (Exception ex)
                        {
                            Echo("Error ACTION :" + commandAction);
                            printErrorDebug(ex);
                        }
                    }
                    else
                    {
                        Echo("Unknown Command " + command);
                    }
                }//END IF ARGUMENT
            }//END IF INIT
        }//END MAIN

        public double countItem(IMyInventory inv, string itemType, string itemSubType)
        {
            var items = new List<MyInventoryItem>();
            inv.GetItems(items, null);
            double total = 0.0f;
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Type.TypeId.ToString().EndsWith(itemType) && items[i].Type.SubtypeId.ToString() == itemSubType)
                {
                    total += (double)items[i].Amount;
                }
            }
            return total;
        }

        public double getExchangeCurrency(string oreChoice, double amount, string oreType)
        {            
            return amount * ressourceList.GetRessourcePrice(oreChoice, oreType);
        }

        //FUNCTIONS
        public void drawInventoryOnLCD(IMyTerminalBlock blockToDraw, IMyTextPanel LCD,bool clean=false)
        {
            LCD.WriteText("CARGO NAME:" + (blockToDraw.CustomNameWithFaction!=""? blockToDraw.CustomNameWithFaction+"\n" : "\n") + blockToDraw.CustomName + "\n", !clean);

            IMyInventory blockInventory = blockToDraw.GetInventory();
            List<MyInventoryItem> blockItems = new List<MyInventoryItem>();
            blockInventory.GetItems(blockItems);

            //CYCLE ITEMS TO SHOW THEM ALL
            foreach (MyInventoryItem item in blockItems)
            {
                string itype = item.Type.ToString();
                LCD.WriteText("Type : " + (item.Type.ToString().Split('_')[1]).Split('/')[0] + " Name : " + item.Type.ToString().Split('/')[1] + " "+item.Amount+ "\n", true);
            }
        }

        public void Refresh()
        {
            string number = _commandLine.Argument(1);
            if (number == null)
            {
                Echo("ARGUMENT NULL!");
            }
            else
            {
                //TYPETEST
                foreach (var blockGroup in TS_BlockGroups)
                {
                    string[] GroupNameString = blockGroup.Name.Split('_');

                    //IF WE GET TRADE CONNECTORS GROUPS
                    if (GroupNameString[0] == "EasyTrade")
                    {
                        if (GroupNameString[1] == "TradeCargo")
                        {
                            if (GroupNameString[2] == number)
                            {
                                //IN GOOD CONNECTOR GROUP
                                
                                //LOOP ALL BLOCKS OF TRADE CONNECTOR GROUPS

                                //Create list of blocks for LCD group
                                List<IMyTextPanel> currentBlockLCDGroup = new List<IMyTextPanel>();
                                blockGroup.GetBlocksOfType<IMyTextPanel>(currentBlockLCDGroup);

                                foreach (IMyTextPanel lcd in currentBlockLCDGroup)
                                {                                    
                                    if (lcd.CustomData == "PlayerChoice")
                                    {
                                        refreshPlayerChoice(lcd);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Trade()
        {
            string number = _commandLine.Argument(1);
            if (number == null)
            {
                Echo("ARGUMENT NULL!");
            }
            else
            {
                //TYPETEST
                foreach (var blockGroup in TS_BlockGroups)
                {
                    string[] GroupNameString = blockGroup.Name.Split('_');

                    //IF WE GET TRADE CONNECTORS GROUPS
                    if (GroupNameString[0] == "EasyTrade")
                    {
                        if (GroupNameString[1] == "TradeCargo")
                        {
                            if (GroupNameString[2] == number)
                            {
                                //IN GOOD CONNECTOR GROUP
                                string connectorCustomData = null;
                                //LOOP ALL BLOCKS OF TRADE CONNECTOR GROUPS

                                //Create list of blocks for LCD group
                                List<IMyTextPanel> currentBlockLCDGroup = new List<IMyTextPanel>();
                                blockGroup.GetBlocksOfType<IMyTextPanel>(currentBlockLCDGroup);

                                foreach (IMyTextPanel lcd in currentBlockLCDGroup)
                                {
                                    TradeConnector_LCD.Add(lcd);
                                    if ( lcd.CustomData == "PlayerChoice" )
                                    {
                                        connectorCustomData = lcd.GetText();
                                    }
                                }                                                            

                                //Create list of blocks for ConveoyrSorter group
                                List<IMyConveyorSorter> currentBlockConveyorGroup = new List<IMyConveyorSorter>();
                                blockGroup.GetBlocksOfType<IMyConveyorSorter>(currentBlockConveyorGroup);

                                foreach (IMyConveyorSorter sorter in currentBlockConveyorGroup)
                                {
                                    TradeConnector_Sorter.Add(sorter);
                                }

                                //Create list of blocks for ConveoyrSorter group
                                List<IMyCargoContainer> currentBlockCargoGroup = new List<IMyCargoContainer>();
                                blockGroup.GetBlocksOfType<IMyCargoContainer>(currentBlockCargoGroup);

                                foreach ( IMyCargoContainer cargo in currentBlockCargoGroup)
                                {
                                    TradeConnector_Cargo.Add(cargo);
                                }

                                //Create list of captors for TradeSensor group
                                List<IMySensorBlock> currentBlockSensorGroup = new List<IMySensorBlock>();
                                blockGroup.GetBlocksOfType<IMySensorBlock>(currentBlockSensorGroup);

                                foreach (IMySensorBlock sensor in currentBlockSensorGroup)
                                {
                                    sensor.ApplyAction("OnOff_On");
                                    TradeConnector_Sensor.Add(sensor);
                                }

                                if (TradeConnector_Sensor.Count() == 1)
                                {

                                    //Write intel on LCD
                                    TradeConnector_Sensor.ElementAt(0).DetectedEntities(entity_list);
                                    MyDetectedEntityInfo entity = entity_list.ElementAt(0);
                                    if (entity_list.Count() == 1)
                                    {
                                        //Write entity datas
                                        DrawOnLCDList(TradeConnector_LCD, "Entity Name : " + entity.Name + "\nEntity id : " + entity.EntityId, false);

                                        //Open SORTER
                                        try
                                        {
                                            if (TradeConnector_Sorter.First() != null)
                                                foreach (IMyConveyorSorter sorter in TradeConnector_Sorter)
                                                {
                                                    sorter.ApplyAction("OnOff_On");
                                                }
                                        }
                                        catch (Exception ex)
                                        {
                                            Echo("Error Open Sorter");
                                            printErrorDebug(ex);
                                        }


                                        //BEGIN TRADE
                                        string[] customDatas = connectorCustomData.Split(new string[] { "\n" }, StringSplitOptions.None);
                                        IMyCargoContainer v0 = TradeConnector_Cargo.ElementAt(0);
                                        string[] tradeCommand = null;
                                        //Get Commands
                                        for (var i = 0; i < customDatas.Count(); i++)
                                        {
                                            if (customDatas[i] == "[DO NOT REMOVE THIS LINE IN BRACKET]")
                                            {
                                                tradeCommand = customDatas[i + 1].Split(' ');
                                            }
                                        }

                                        //Get player money & add if new
                                        double playerExchangeCurrency = GetPlayerMoney(entity.Name);
                                        double tradeMoney = 0;

                                        if (playerExchangeCurrency == 0)
                                        {
                                            userList.Add(new User(entity.EntityId, entity.Name, 0));
                                        }

                                        //Check Trade Command & Entity list number
                                        if (tradeCommand[0] == "TradeCommand")
                                        {
                                            tradeCommand[1].ToLower();

                                            switch (tradeCommand[1])
                                            {
                                                case "sell":
                                                    playerExchangeCurrency += PlayerSell();
                                                    break;
                                                case "buy":
                                                    // Look choice and buy

                                                    DrawOnLCDList(TradeConnector_LCD, "\nIngot buy choice :", true);

                                                    for (var i = 0; i < customDatas.Count(); i++)
                                                    {
                                                        if (customDatas[i] == "[DO NOT REMOVE THIS LINE IN BRACKET]")
                                                        {
                                                            for (int j = i + 2; j < customDatas.Count(); j++)
                                                            {
                                                                if (customDatas[j] != "")
                                                                {
                                                                    double amountToPay = 0;
                                                                    string ore = (customDatas[j].Split(' ')[0]).ToLower();
                                                                    string ingotName = CapitalizeFirstLetter(ore);
                                                                    string type = CapitalizeFirstLetter((customDatas[j].Split(' ')[1]).ToLower());
                                                                    string amountS = customDatas[j].Split(' ')[2];
                                                                    double amountIngot = Convert.ToDouble(amountS);

                                                                    amountToPay = amountIngot * ressourceList.GetRessourcePrice(ingotName, type);
                                                                    DrawOnLCDList(TradeConnector_LCD, "\n" + amountIngot + " " + type + " " + ingotName + " Cost : " + amountToPay + " $TS, " + ressourceList.GetRessourcePrice(ingotName, type) + " $TS/unit.", true);

                                                                    if (playerExchangeCurrency >= amountToPay)
                                                                    {
                                                                        playerExchangeCurrency -= amountToPay;
                                                                        TransfertFromCoreCargo(v0, amountIngot, type, ingotName);
                                                                        DrawOnLCDList(TradeConnector_LCD, "\n" + type + " of " + ingotName + " transfered ! You have " + playerExchangeCurrency + " $TS left.", true);
                                                                    }
                                                                    else
                                                                    {
                                                                        DrawOnLCDList(TradeConnector_LCD, "\nYou don't have enough $TS !You can afford " + playerExchangeCurrency / ressourceList.GetRessourcePrice(ingotName, type) + " of " + type + " " + ingotName, true);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "trade":
                                                    //Check that % is 100
                                                    double security_1 = 0;
                                                    for (var i = 0; i < customDatas.Count(); i++)
                                                    {
                                                        if (customDatas[i] == "[DO NOT REMOVE THIS LINE IN BRACKET]")
                                                        {
                                                            for (int j = i + 2; j < customDatas.Count(); j++)
                                                            {
                                                                string amountS = customDatas[j].Split(' ')[2];
                                                                security_1 += Convert.ToDouble(amountS);

                                                            }
                                                        }
                                                    }

                                                    //IF PERCENT ARE NOT 100
                                                    if (security_1 == 100)
                                                    {

                                                        //Sell Connector stuff
                                                        tradeMoney += PlayerSell();
                                                        //Look choice and buy
                                                        DrawOnLCDList(TradeConnector_LCD, "\nIngot trade choice :", true);
                                                        double tempMoney = 0;
                                                        //Check % under 100


                                                        for (var i = 0; i < customDatas.Count(); i++)
                                                        {
                                                            if (customDatas[i] == "[DO NOT REMOVE THIS LINE IN BRACKET]")
                                                            {
                                                                for (int j = i + 2; j < customDatas.Count(); j++)
                                                                {
                                                                    if (customDatas[j] != "")
                                                                    {
                                                                        string ore = (customDatas[j].Split(' ')[0]).ToLower();
                                                                        string ingotName = CapitalizeFirstLetter(ore);

                                                                        string type = CapitalizeFirstLetter((customDatas[j].Split(' ')[1]).ToLower());

                                                                        string amountS = customDatas[j].Split(' ')[2];
                                                                        double amountPercent = Convert.ToDouble(amountS);

                                                                        //Give items !
                                                                        double amountToGive = 0;
                                                                        double countItems = 0;
                                                                        amountToGive = (tradeMoney * (amountPercent / 100)) / ressourceList.GetRessourcePrice(ingotName, type);
                                                                        TransfertFromCoreCargo(v0, amountToGive, type, ingotName);
                                                                        //Check and calculate real value obtained
                                                                        countItems = countItem(v0.GetInventory(0), type, ingotName);
                                                                        tempMoney += countItems * ressourceList.GetRessourcePrice(ingotName, type);
                                                                        DrawOnLCDList(TradeConnector_LCD, "\nYou got " + countItems + " of " + ingotName + " " + type + " for " + countItems * ressourceList.GetRessourcePrice(ingotName, type) + " $TS, " + ressourceList.GetRessourcePrice(ingotName, type) + " $TS/unit.", true);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        tradeMoney -= tempMoney;
                                                        DrawOnLCDList(TradeConnector_LCD, "\nIngots cost you " + tempMoney + " $TS", true);
                                                        DrawOnLCDList(TradeConnector_LCD, "\nYou have " + (playerExchangeCurrency += tradeMoney) + " $TS", true);
                                                    }
                                                    else
                                                    {
                                                        DrawOnLCDList(TradeConnector_LCD, "\nTRADE CANCELLED ! Your choices need to make 100 %", true);
                                                    }
                                                    break;
                                                default:
                                                    DrawOnLCDList(TradeConnector_LCD, "\nTRADE CANCELLED ! Bad Command :" + tradeCommand[1], true);
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            DrawOnLCDList(TradeConnector_LCD, "\nTRADE CANCELLED ! Bad TradeCommand needed, exact name.", true);
                                        }


                                        //Close SORTER
                                        try
                                        {
                                            if (TradeConnector_Sorter.First() != null)
                                                foreach (IMyConveyorSorter sorter in TradeConnector_Sorter)
                                                {
                                                    sorter.ApplyAction("OnOff_Off");
                                                }
                                        }
                                        catch (Exception ex)
                                        {
                                            Echo("Error Close Sorter");
                                            printErrorDebug(ex);
                                        }

                                        //Set new money for player
                                        SetPlayerMoney(entity.Name, playerExchangeCurrency);
                                        foreach (IMyTextPanel panel in TradeConnector_LCD)
                                        {
                                            if (panel.CustomData == "DrawCargo")
                                            {
                                                panel.WriteText("");
                                                drawInventoryOnLCD(TradeConnector_Cargo.ElementAt(0), panel, true);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DrawOnLCDList(TradeConnector_LCD, "\nTRADE CANCELLED ! You are " + entity_list.Count() + " near button, need to be alone/1", true);
                                    }
                                }
                                else
                                {
                                    Echo("YOU NEED A SENSOR IN YOUR GROUP !! : " + blockGroup.Name);
                                }
                            }
                        }
                    }
                }
            }
        }

        //Transfert item to core cargo
        public bool TransfertToCoreCargo(IMyCargoContainer connector, double amount, string type, string subtype)
        {
            IMyCargoContainer myCargo = TradeStation_CoreCargo.ToArray()[0];
            IMyInventory cargo_inventory = myCargo.GetInventory();
            IMyInventory connector_inventory = connector.GetInventory();
            bool returner = true;
            //OPEN CARGO SORTER
            try
            {
                if (TradeStation_Core_Sorter.First() != null)
                    foreach (IMyConveyorSorter sorter in TradeStation_Core_Sorter)
                    {
                        sorter.ApplyAction("OnOff_On");
                    }
            }
            catch (Exception ex)
            {
                Echo("Error Open CORE Sorter");
                printErrorDebug(ex);
            }

            if (connector_inventory.CanTransferItemTo(cargo_inventory, new MyItemType("MyObjectBuilder_" + type, subtype)))
            {
                List<MyInventoryItem> connectorItems = new List<MyInventoryItem>();
                connector_inventory.GetItems(connectorItems);

                foreach (MyInventoryItem item in connectorItems)
                {
                    if (item.Type.ToString() == "MyObjectBuilder_" + type + "/" + subtype)
                    {
                        returner = connector_inventory.TransferItemTo(cargo_inventory, item,(MyFixedPoint) amount);
                    }
                }
            }
            else
            {
                Echo($"CANNOT TRANSFERT TO {myCargo.CustomName}");
                returner = false;
            }

            //CLOSE CARGO SORTER
            try
            {
                if (TradeStation_Core_Sorter.First() != null)
                    foreach (IMyConveyorSorter sorter in TradeStation_Core_Sorter)
                    {
                        sorter.ApplyAction("OnOff_Off");
                    }
            }
            catch (Exception ex)
            {
                Echo("Error Close CORE Sorter");
                printErrorDebug(ex);
            }

            return returner;
        }

        public bool TransfertFromCoreCargo(IMyCargoContainer connector, double amount, string type, string subtype)
        {
            bool returner = true;
            
            //OPEN CARGO SORTER
            try
            {
                if (TradeStation_Core_Sorter.First() != null)
                    foreach (IMyConveyorSorter sorter in TradeStation_Core_Sorter)
                    {
                        sorter.ApplyAction("OnOff_On");
                    }
            }
            catch (Exception ex)
            {
                Echo("Error Open CORE Sorter");
                printErrorDebug(ex);
            }

            foreach (IMyCargoContainer myCargo in TradeStation_CoreCargo)
            {
                //IMyCargoContainer myCargo = TradeStation_CoreCargo.ElementAt(0);
                IMyInventory cargo_inventory = myCargo.GetInventory();
                IMyInventory connector_inventory = connector.GetInventory();



                if (cargo_inventory.CanTransferItemTo(connector_inventory, new MyItemType("MyObjectBuilder_" + type, subtype)))
                {
                    if (cargo_inventory.ContainItems((MyFixedPoint) amount, new MyItemType("MyObjectBuilder_" + type, subtype)))
                    {
                        List<MyInventoryItem> cargoItems = new List<MyInventoryItem>();
                        cargo_inventory.GetItems(cargoItems);

                        foreach (MyInventoryItem item in cargoItems)
                        {
                            if (item.Type.ToString() == "MyObjectBuilder_" + type + "/" + subtype)
                            {
                                return cargo_inventory.TransferItemTo(connector_inventory, item, (MyFixedPoint) amount);
                            }
                        }
                    }
                    else
                    {
                        MyFixedPoint availableAmount = cargo_inventory.GetItemAmount(new MyItemType("MyObjectBuilder_" + type, subtype));
                        if (availableAmount > 0)
                        {
                            DrawOnLCDList(TradeConnector_LCD, "\nWE CAN TRANSFERT " + availableAmount.ToString() + " " + type + " " + subtype, true);
                            //TRANSFERT AVAILABLE AMOUNT
                            List<MyInventoryItem> cargoItems = new List<MyInventoryItem>();
                            cargo_inventory.GetItems(cargoItems);

                            foreach (MyInventoryItem item in cargoItems)
                            {
                                if (item.Type.ToString() == "MyObjectBuilder_" + type + "/" + subtype)
                                {
                                    returner = cargo_inventory.TransferItemTo(connector_inventory, item, availableAmount);
                                    amount -= (double)availableAmount;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Echo($"CANNOT TRANSFERT FROM {myCargo.CustomName}");
                    DrawOnLCDList(TradeConnector_LCD, "\nCANNOT TRANSFERT, PATH BROKEN TO " + myCargo.CustomName+"\nOR NO MORE ITEMS INTOR CARGO.", true);
                    returner= false;
                }
            }

            //CLOSE CARGO SORTER
            try
            {
                if (TradeStation_Core_Sorter.First() != null)
                    foreach (IMyConveyorSorter sorter in TradeStation_Core_Sorter)
                    {
                        sorter.ApplyAction("OnOff_Off");
                    }
            }
            catch (Exception ex)
            {
                Echo("Error Close CORE Sorter");
                printErrorDebug(ex);
            }

            return returner;
        }


        public bool drawBlockDebug(IMyTerminalBlock block, IMyTextPanel LCD)
        {
            List<ITerminalProperty> properties = new List<ITerminalProperty>();
            List<ITerminalAction> actions = new List<ITerminalAction>();
            block.GetProperties(properties);
            block.GetActions(actions);

            //Print PROPERTIES
            LCD.WriteText("BLOCK NAME :" + block.CustomName + "\nPROPERTIES : \n");

            foreach (var property in properties)
            {
                LCD.WriteText(property.Id + "\n", true);
            }

            //Print ACTIONS
            LCD.WriteText("BLOCK NAME :" + block.CustomName + "\nACTIONS : \n", true);
            foreach (var action in actions)
            {
                LCD.WriteText(action.Id + ": " + action.Name + "\n", true);
            }
            return true;
        }


        //Launch a command on a group.
        public bool launchActionOnGroup(string blockGroupName, string action)
        {
            try
            {
                IMyBlockGroup blockGroup = GridTerminalSystem.GetBlockGroupWithName(blockGroupName);
                List<IMyTerminalBlock> Myblocks = new List<IMyTerminalBlock>();
                blockGroup.GetBlocks(Myblocks);

                //Loop with all blocks in group
                for (var blockCount = 0; blockCount < Myblocks.Count; blockCount++)
                {
                    //drawBlockDebug(Myblocks[blockCount], LCD3);
                    try
                    {
                        Myblocks[blockCount].ApplyAction(action);
                    }
                    catch (Exception ex)
                    {
                        Echo("Error Handled in launchActionOnGroup, bad action name : " + action + "\nException msg:" + ex.Message);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Echo("ERROR MAIN LOOP launchActionOnGroup");
                Echo("Error " + ex.ToString());
                return false;
            }

            return true;
        }

        public void writeRuntime(IMyTextPanel LCD)
        {
            LCD.WriteText("Cycles : " + cycleCount);
            LCD.WriteText("\nLast Run time ms:" + Runtime.LastRunTimeMs, true);
            LCD.WriteText("\nTime since last :" + Runtime.TimeSinceLastRun, true);
            LCD.WriteText("\nMaxInstructionCount :" + Runtime.MaxInstructionCount, true);
            LCD.WriteText("\nUpdateFrequency :" + Runtime.UpdateFrequency, true);
            LCD.WriteText("\nSTORAGE :" + Storage, true);
        }

        public void writeDetails(IMyFunctionalBlock block, IMyTextPanel LCD)
        {
            string[] _detail = (block.DetailedInfo).Split(new string[] { "\n" }, StringSplitOptions.None);
            for (var i = 0; i < _detail.Count(); i++)
            {
                if (i == 0)
                    LCD.WriteText(_detail[i]);
                else
                    LCD.WriteText(_detail[i], true);
            }
        }

        public double PlayerSell()
        {
            //CALCULATE AMOUNT TO SELL FROM CARGOS
            // Check ressources and count them for exchange
            IMyCargoContainer v0 = TradeConnector_Cargo.ElementAt(0);
            double playerExchangeCurrency = 0;

            foreach ( RessourceToken tok in ressourceList.ressourceList )
            {
                double amountToSell = countItem(v0.GetInventory(0), tok.Type, tok.Name);
                playerExchangeCurrency += getExchangeCurrency(tok.Name, amountToSell, tok.Type);

                if (amountToSell != 0.0)
                {
                    if ( TransfertToCoreCargo(v0, amountToSell, tok.Type, tok.Name) )
                    {
                        DrawOnLCDList(TradeConnector_LCD, "\nYou sold " + amountToSell + " of " + tok.Type + " " + tok.Name, true);
                    }
                    else
                    {
                        DrawOnLCDList(TradeConnector_LCD, "\nTHERE WAS A PROBLEM DURING TRANSFERT! ABORTED !", true);
                    }
                }
            }

            DrawOnLCDList( TradeConnector_LCD, "\nYou get " + playerExchangeCurrency + " $TS", true);

            return playerExchangeCurrency;
        }

        public double GetPlayerMoney(string name)
        {                 
            foreach ( User user in userList )
            {
                if ( user.Name == name )
                {
                    if (user.Money == 0)
                        return 1;
                    else
                        return user.Money;
                }
            }
            return 0;
        }

        public void SetPlayerMoney(string name, double money)
        {
            for (int i = 0; i < userList.Count(); i++)
            {
                if (userList.ElementAt(i).Name == name)
                {
                    userList.ElementAt(i).Money = money;                    
                }
            }

            //WRITE INTO PANELS
            foreach ( IMyTextPanel txt in UserList_LCD )
            {
                txt.WriteText("USER LIST:\n");
            }
            
            for (int j = 0; j < userList.Count(); j++)
            {
                //Cycle screens to write
                foreach (IMyTextPanel txt in UserList_LCD)
                {
                    txt.WriteText(userList.ElementAt(j).Name + ":" + userList.ElementAt(j).Money + ":$TS ID:" + userList.ElementAt(j).Id + "\n", true);
                }
            }
        }

        public void printErrorDebug(Exception ex)
        {
            Echo("Error msg:" + ex.Message);
            Echo("Error src:" + ex.Source);
            Echo("Error exc:" + ex.InnerException);
            Echo("Error stack:" + ex.StackTrace);
        }

        public void DrawOnLCDList(List<IMyTextPanel> LCD_list, string msg, bool append = false )
        {
            try
            {
                foreach( IMyTextPanel panel in LCD_list )
                {
                    if (panel.CustomData != "PlayerChoice")
                    {
                        panel.WriteText(msg, append);
                    }
                }
            }
            catch(Exception ex)
            {
                Echo("Error in LCD_List");
                printErrorDebug(ex);
            }
        }

        public string CapitalizeFirstLetter(string toCap)
        {
            return (string)(char.ToUpper(toCap.ToCharArray()[0]) + toCap.Substring(1));
        }

        #region mdk preserve
        /* MODIFY THESES LINE TO CHANGE LCD PLAYER CHOICE
         * LEAVE THE LAST LINE IN BRACKET WITH COMMANDS
         */
        public void refreshPlayerChoice(IMyTextPanel lcd)
        {
            lcd.WriteText("1:Put your ressources into the cargo, name is on screens\n");
            lcd.WriteText("2:You have 3 TradeCommand : BUY / SELL / TRADE\n", true);
            lcd.WriteText("Example for 'TRADE' command:\n", true);
            lcd.WriteText("Then On separated lines you write ressource name and\n", true);
            lcd.WriteText("a number as % of value of your ingots\n", true);
            lcd.WriteText("For example: cobalt ingot 40 and nickel ingot 60\n", true);
            lcd.WriteText("4:To buy you write 'buy' as TradeCommand then\n", true);
            lcd.WriteText("write ressource name and ingots number\n",true);
            lcd.WriteText("For example: silver ingot 125\n", true);
            lcd.WriteText("5:To sell you write 'sell' as TradeCommand and no ressource line\n", true);
            lcd.WriteText("[DO NOT REMOVE THIS LINE IN BRACKET]\n", true);
            lcd.WriteText("TradeCommand trade\n", true);
            lcd.WriteText("cobalt ingot 40\n", true);
            lcd.WriteText("nickel ingot 60", true); ;
        }
        
        //PLACE FOR FUNCTIONS
        //END PLACE FOR FUNCTIONS
        #endregion
    }
}


