﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        public class User
        {
            public double Money { get; set; }

            public string Name { get; set; }

            public int ArrayIndice { get; set; }

            public long Id { get; set; }

            public User(string aname,double amoney)
            {
                Name = aname;
                Money = amoney;
            }

            public User(string aname)
            {
                Name = aname;
            }

            public User(long aid, string aname)
            {
                Name = aname;
                Id = aid;
            }

            public User(long aid, string aname, double amoney)
            {
                Name = aname;
                Id = aid;
                Money = amoney;
            }

            public User(string aname, double amoney, int arrayI)
            {
                Name = aname;
                Money = amoney;
                ArrayIndice = arrayI;
            }

            public User(long aid, string aname, double amoney, int arrayI)
            {
                Name = aname;
                Money = amoney;
                ArrayIndice = arrayI;
                Id = aid;
            }
        }
    }
}